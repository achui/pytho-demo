# -*- coding:UTF-8 -*-
from datetime import datetime


class DateUtil:

    @staticmethod
    def formatFromTimeStamp(timestamp, patten="%m/%d/%Y"):
        return datetime.utcfromtimestamp(timestamp).strftime(patten)

    @staticmethod
    def format(date: datetime,  patten="%m/%d/%Y"):
        return date.strftime(patten)
