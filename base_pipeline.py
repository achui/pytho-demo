#!/usr/bin/env python
# -*- coding: utf-8 -*-

class ItemPipeline(object):
    def process_item(self, item):
        raise NotImplementedError
