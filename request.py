#!/usr/bin/env python
# encoding: utf-8


class Request(object):
    def __init__(self, url=None, callback=None, headers=None, verify=False, method="get", params=None, supportJs=False,
                 timeout=30, useProxy=False, proxies=None):
        self.url = url
        self.callback = callback
        self.headers = headers
        self.verify = verify
        self.method = method
        self.params = params
        self.supportJs = supportJs
        self.timeout = timeout
        self.userProxy = useProxy
        self.proxies = proxies
