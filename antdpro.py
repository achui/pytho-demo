#!/usr/bin/env python
# encoding: utf-8
from bs4 import BeautifulSoup
from spider import Spider
from console_pipeline import ConsolePipeline


class AntdProProcess(object):
    start_url = "https://preview.pro.ant.design/user/login"
    supportJs = True
    timeout = 500
    headers = None

    def process(self, html):
        items = {}
        soup = BeautifulSoup(html, 'lxml')
        print(soup.head.title.text)
        elements = soup.find_all(class_="antd-pro-pages-user-login-other")
        print(elements)
        for element in elements:
            print(element.text)
            yield element

if __name__ == "__main__":
    Spider(AntdProProcess()).addPipeline(ConsolePipeline()).start()
