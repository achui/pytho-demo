#!/usr/bin/env python
# -*- coding: utf-8 -*-
from base_pipeline import ItemPipeline
from pymongo import MongoClient
from settings import DB_MONGO_HOST, DB_MONGO_USER, DB_MONGO_PWD


class MongoDBPipeline(ItemPipeline):
    def __init__(self, database, collection):
        self.__client = MongoClient(DB_MONGO_HOST, username=DB_MONGO_USER, password=DB_MONGO_PWD, authSource=database)
        self.__db = self.__client[database]
        self.__collection = collection

    def process_item(self, item):
        if item is not None:
            self.__db[self.__collection].insert_one(item)


