#!/usr/bin/env python
# -*- coding: utf-8 -*-
from base_pipeline import ItemPipeline
import json


class JsonConsolePipeline(ItemPipeline):
    def process_item(self, item):
        print("-"*80)
        print(json.dumps(item).encode('utf8').decode("unicode-escape"))
        #print(item)
        print("#"*80)
