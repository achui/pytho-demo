    # pytho-demo

#### Description
网络爬虫，支持多线程，JS，动态代理，可配置


#### Installation

1. xxxx
2. xxxx
3. xxxx

#### 开发流程

1. 普通开发：创建爬虫Process类型，实现process方法，例子看参考mytest.py爬取内涵漫画图片
2. 配置开发：只要创建配置文件，如neihan.json即可，配置参数说明如下：

| 参数 | 说明 | 例子 |
| :------| :------: | :------: |
| url | 需要爬取的地址，支持python中的模板语法 | 稍微长一点的文本 |
| range | 需要爬取的范围 e.g 1-10表示抓取1-10页 | 中等文本 |

#### Contribution

1. Fork the project
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [http://git.mydoc.io/](http://git.mydoc.io/)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)