import asyncio
from pyppeteer import launch
import time

# async def main():
#     browser = await launch(headless=True)
#     page = await browser.newPage()
#     await page.goto('https://www9.qa.ehealthinsurance.com/bov2/login.html')
#     await page.type("#userId", "portz")
#     await page.type("#password", "Achui_19800724")
#     inputElement = await page.querySelector('#BOLogin')
#     await inputElement.click()
#     await  page.waitForNavigation()
#     await page.goto("https://www9.qa.ehealthinsurance.com/bov2/index.html#/liveopsmapping?t=81290", options={"waitUntil":"networkidle2"})
#     #await  page.waitForXPath(xpath='//*[@id="main-container"]/div/div/div[2]/div[2]/table/tbody/tr[10]/td[3]')
#     content = await page.content()
#     print(content)
#     #await browser.close()
#
# asyncio.get_event_loop().run_until_complete(main())

import asyncio
from pyppeteer import launch

async def main():
    browser = await launch(args=['--no-sandbox', '--disable-setuid-sandbox'], headless=True)
    page = await browser.newPage()
    page.setDefaultNavigationTimeout(6000000)
    await page.goto('https://www.baidu.com')
    await page.screenshot({'path': 'example.png'})
    await browser.close()

asyncio.get_event_loop().run_until_complete(main())