#!/usr/bin/env python
# encoding: utf-8
from random import choice

import requests
from requests_html import HTMLSession

from logger import logger
from settings import PROXY_POOL_FILE


class RequestDownloader(object):
    def __init__(self, request=None):
        super().__init__()
        self.request = request
        self.status_code = 200

    def __getProxyIP(self):
        with open(PROXY_POOL_FILE) as f:
            content = f.read().splitlines()
        return choice(content)

    def process_request(self):
        url = self.request.url
        if not self.request.headers:
            header = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.119 Safari/537.36'
            }
        else:
            header = self.request.headers
        # handle proxyIP
        if self.request.userProxy:
            proxyIP = self.__getProxyIP()
            if proxyIP.startswith("https"):
                proxies = {'https': proxyIP}
            else:
                proxies = {'http': proxyIP}
            logger.info("using proxy ip:" + proxyIP)
        else:
            proxies = None
        if self.request.proxies:
            proxies = self.request.proxies
        session = requests.session()
        if self.request.supportJs:
            session = HTMLSession()
        session.proxies = proxies
        if self.request.method == 'get':
            response = session.get(url, headers=header, verify=self.request.verify)
        if self.request.method == 'post':
            if header["Content-Type"] == "application/json":
                response = session.post(url, headers=header, json=self.request.params, verify=self.request.verify)
            else:
                response = session.post(url, headers=header, data=self.request.params, verify=self.request.verify)
        # logger.debug("response staus code from url %s is :%d:" % (url, response.status_code))
        self.status_code = response.status_code
        if response.status_code == 200:
            response.encoding = "UTF-8"
            html = response.text
            if self.request.supportJs:
                response.html.render(timeout=self.request.timeout)
                html = response.html.html
        else:
            html = "<html><head><title>" + str(response.status_code) + "</title></head></html>"
        return html
