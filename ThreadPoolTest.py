#!/usr/bin/env python
# -*- coding: utf-8 -*-
import concurrent
from concurrent.futures import ThreadPoolExecutor
import random
import time


def add(n):
    time.sleep(random.randint(1, 2))
    print("*" * 20, n)
    return n ** 2


def calculateParallel(numbers):
    with ThreadPoolExecutor(max_workers=10) as executor:
        #future_to_url = {executor.submit(add, ab): ab for ab in numbers}
        for ab in numbers:
            f = executor.submit(add, ab)
            #print(f.result())



if __name__ == "__main__":
    numbers = [];
    for i in range(51):
        numbers.append(i)
    calculateParallel(numbers)
