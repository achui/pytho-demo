#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import logging
import logging.config
from settings import LOGGING
import os

if not os.path.exists("log"):
    os.mkdir("log")
logger = logging.getLogger(__name__)
BASE_LOG_DIR = "log"
logging.config.dictConfig(LOGGING)
