#!/usr/bin/env python
# encoding: utf-8

import requests
from bs4 import BeautifulSoup
from lxml import etree
import aiohttp
import asyncio


def getPage(url):
    response = requests.get(url)
    if response.status_code == 200:
        response.encoding = 'utf-8'
        return response.text
    return None


def parserPage(text):
    html = etree.HTML(text)
    result = html.xpath('//div[@class="perContentBox"]')
    for item in result:
        value = item.xpath('*/a/@href')
        detailHtml = getPage(value[0])
        parserDetailPage(detailHtml)
        #print(detailHtml.replace(u'\xa9', u''))
        #print(unicode("detailHtml", encoding="utf-8"))


def parserDetailPage(text):
    html = etree.HTML(text)
    result = html.xpath('//div[@class="conBox"]/p[1]')
    print('result:', result[0].text)


def main():
    html = getPage('http://www.msmpy.com/')
    parserPage(html)


main()
