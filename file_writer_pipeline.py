#!/usr/bin/env python
# -*- coding: utf-8 -*-
import requests
from contextlib import closing
import os

from base_pipeline import ItemPipeline
from downloader import ProgressBar


class FileWriterPipeline(ItemPipeline):
    def __init__(self, path="default", filename="default.txt"):
        super(FileWriterPipeline, self).__init__()
        self.__path = path
        self.__filename = filename
        is_exist = os.path.exists(path)
        if not is_exist:
            os.mkdir(path)

    def process_item(self, item):
        if item is None:
            return
        content = item["content"]
        filepath = "./%s/%s" % (self.__path, self.__filename)
        with open(filepath, "a") as file:
            file.write(content)
